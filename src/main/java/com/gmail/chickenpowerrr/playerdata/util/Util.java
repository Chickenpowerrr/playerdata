package com.gmail.chickenpowerrr.playerdata.util;

/**
 * This class contains some useful methods which might be nice
 * to use in several classes.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public final class Util {

  public static final long TICKS_PER_SECOND = 20L;

  /**
   * A hacky way of throwing checked {@link Exception}s as unchecked {@link Exception}.
   *
   * @param throwable the {@link Throwable} to throw
   * @param <T> the type to make the hack work
   * @param <U> the fake return type
   * @return nothing
   * @throws T the given {@link Throwable}
   */
  @SuppressWarnings("unchecked")
  public static <T extends Throwable, U> U sneakyThrow(Throwable throwable) throws T {
    throw (T) throwable;
  }

  /**
   * A hacky way to get rid of the checked {@link Exception}s and
   * throw them as unchecked {@link Exception}.
   *
   * @param sneakyExecutor what should be executed
   */
  public static void sneakyExecute(SneakyExecutor sneakyExecutor) {
    try {
      sneakyExecutor.execute();
    } catch (Exception e) {
      sneakyThrow(e);
    }
  }

  /**
   * Converts a tick value to seconds.
   *
   * @param ticks the time in ticks
   * @return the time in seconds
   */
  public static long ticksToSeconds(long ticks) {
    return ticks / TICKS_PER_SECOND;
  }

  /**
   * Converts a second value to ticks.
   *
   * @param seconds the time in seconds
   * @return the time in tick
   */
  public static long secondsToTicks(long seconds) {
    return seconds * TICKS_PER_SECOND;
  }

  /**
   * Used to define an action which can be performed.
   *
   * @author Mark van Wijk
   * @since 1.0.0
   */
  @FunctionalInterface
  public interface SneakyExecutor {

    /**
     * Executes the action.
     *
     * @throws Exception if something goes wrong
     */
    void execute() throws Exception;
  }

  /**
   * Prevent instantiations.
   */
  private Util() {

  }
}
