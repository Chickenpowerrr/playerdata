package com.gmail.chickenpowerrr.playerdata;

import com.gmail.chickenpowerrr.playerdata.data.DataManager;
import com.gmail.chickenpowerrr.playerdata.data.source.DataSource;
import com.gmail.chickenpowerrr.playerdata.data.source.DataSourceFactory;
import com.gmail.chickenpowerrr.playerdata.listener.PlayerJoinEventListener;
import com.gmail.chickenpowerrr.playerdata.listener.PlayerMoveEventListener;
import com.gmail.chickenpowerrr.playerdata.listener.PlayerQuitEventListener;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class of the plugin which will start the
 * parts which will handle the logic.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerDataPlugin extends JavaPlugin {

  private DataManager dataManager;

  /**
   * Forces a config to be generated and starts the listeners.
   */
  @Override
  public void onEnable() {
    saveDefaultConfig();

    DataSource dataSource = DataSourceFactory.createDataSource(getConfig());
    dataSource.setUp();
    this.dataManager = new DataManager(this, dataSource);

    Bukkit.getPluginManager().registerEvents(new PlayerJoinEventListener(this.dataManager), this);
    Bukkit.getPluginManager().registerEvents(new PlayerMoveEventListener(this.dataManager), this);
    Bukkit.getPluginManager().registerEvents(new PlayerQuitEventListener(this.dataManager), this);
  }

  /**
   * Closes the used resources.
   */
  @Override
  public void onDisable() {
    try {
      this.dataManager.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
