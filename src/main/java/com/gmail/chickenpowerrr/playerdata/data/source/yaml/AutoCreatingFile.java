package com.gmail.chickenpowerrr.playerdata.data.source.yaml;

import java.io.File;
import java.io.IOException;

/**
 * This class is a file that gets automatically generated if
 * it didn't exist already as soon as the object has been created.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class AutoCreatingFile extends File {

  private static final long serialVersionUID = 5940506766256199865L;

  /**
   * Creates the directories and the file the object represents.
   *
   * @param basePath the path to the file
   * @param fileName the name of the file
   */
  public AutoCreatingFile(String basePath, String fileName) {
    super(basePath, fileName);

    try {
      getParentFile().mkdirs();
      createNewFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Saves the file with the right data.
   */
  public void save() {

  }
}