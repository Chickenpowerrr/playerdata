package com.gmail.chickenpowerrr.playerdata.data.source.sql;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class handles the basic logic for a SQL transaction by saving the
 * previous states before updating them and allowing the previous state to be reset.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class TransactionHandler {

  private final Connection connection;
  private final int previousIsolationLevel;
  private final boolean previousAutoCommit;

  /**
   * Instantiates a new handler, based on the target connection.
   *
   * @param connection the {@link Connection} which will be used
   * @throws SQLException if something goes wrong
   * @see Connection#getAutoCommit()
   * @see Connection#getTransactionIsolation()
   */
  public TransactionHandler(Connection connection) throws SQLException {
    this.connection = connection;
    this.previousAutoCommit = connection.getAutoCommit();
    this.previousIsolationLevel = connection.getTransactionIsolation();
  }

  /**
   * Updates the {@link Connection} to set the given parameters.
   *
   * @param isolationLevel the new isolation level of the {@link Connection}
   * @param autoCommit the new auto commit setting of the {@link Connection}
   * @throws SQLException if something goes wrong
   * @see Connection#setAutoCommit(boolean)
   * @see Connection#setTransactionIsolation(int)
   */
  public void apply(int isolationLevel, boolean autoCommit) throws SQLException {
    this.connection.setAutoCommit(autoCommit);
    this.connection.setTransactionIsolation(isolationLevel);
  }

  /**
   * Sets the auto commit and isolation level back to the previously defined values.
   *
   * @throws SQLException if something goes wrong
   * @see Connection#setAutoCommit(boolean)
   * @see Connection#setTransactionIsolation(int)
   */
  public void revert() throws SQLException {
    this.connection.setAutoCommit(this.previousAutoCommit);
    this.connection.setTransactionIsolation(this.previousIsolationLevel);
  }
}
