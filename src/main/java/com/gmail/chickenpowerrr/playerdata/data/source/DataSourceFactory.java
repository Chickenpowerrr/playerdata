package com.gmail.chickenpowerrr.playerdata.data.source;

import com.gmail.chickenpowerrr.playerdata.data.source.sql.SqlDataSource;
import com.gmail.chickenpowerrr.playerdata.data.source.yaml.YamlDataSource;
import com.gmail.chickenpowerrr.playerdata.data.source.yaml.YamlFile;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * This factory provides an easy way to convert a Spigot {@link Configuration}
 * into a {@link DataSource} which can be used to store and load data.
 *
 * @author Mark van Wijk
 */
public final class DataSourceFactory {

  /**
   * Returns a {@link DataSource} based on the values provided in the {@link Configuration}.
   *
   * @param config the {@link Configuration} which contains the information about the
   *               {@link DataSource} which should be used
   * @return the {@link DataSource} which has been formed based on the given {@link Configuration}
   */
  public static DataSource createDataSource(Configuration config) {
    if (config.getBoolean("data.use-mysql")) {
      return new SqlDataSource(config.getString("data.mysql.host"),
          config.getInt("data.mysql.port"), config.getString("data.mysql.username"),
          config.getString("data.mysql.password"), config.getString("data.mysql.database"),
          config.getInt("data.mysql.max-pool-size"));
    } else {
      return new YamlDataSource(new YamlFile(
          JavaPlugin.getProvidingPlugin(DataSourceFactory.class).getDataFolder().getPath(),
          config.getString("data.yaml.file-name")));
    }
  }

  /**
   * Prevent instantiations.
   */
  private DataSourceFactory() {

  }
}
