package com.gmail.chickenpowerrr.playerdata.data.player;

import java.util.UUID;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * This class represents the data of a {@link Player} on
 * the server.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public interface PlayerData {

  /**
   * Returns the {@link UUID} of the {@link Player}.
   */
  UUID getUuid();

  /**
   * Returns the {@link Location} of the {@link Player} on the server.
   */
  Location getLocation();

  /**
   * Returns the {@link GameMode} in which the {@link Player} is playing
   * on the server.
   */
  GameMode getGameMode();

  /**
   * Returns for how long {@link Player} has played on the server in seconds.
   */
  long getTimePlayed();

  /**
   * Returns {@code true} if the {@link Player} is flying on the server,
   * {@code false} otherwise.
   */
  boolean isFlying();
}
