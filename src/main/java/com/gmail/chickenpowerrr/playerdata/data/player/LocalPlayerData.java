package com.gmail.chickenpowerrr.playerdata.data.player;

import com.gmail.chickenpowerrr.playerdata.util.Util;
import java.util.UUID;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;

/**
 * This {@link PlayerData} is a live updating version of the
 * {@link PlayerData} by using the {@link Player} as the source.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class LocalPlayerData implements PlayerData {

  private final Player player;

  /**
   * Instantiates the {@link PlayerData} with the given {@link Player}
   * as its source.
   *
   * @param player the {@link Player} which will be used as the source
   */
  public LocalPlayerData(Player player) {
    this.player = player;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public GameMode getGameMode() {
    return this.player.getGameMode();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Location getLocation() {
    return this.player.getLocation();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long getTimePlayed() {
    return Util.ticksToSeconds(this.player.getStatistic(Statistic.PLAY_ONE_TICK));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UUID getUuid() {
    return this.player.getUniqueId();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFlying() {
    return this.player.isFlying();
  }
}
