package com.gmail.chickenpowerrr.playerdata.data.source;

import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import java.io.Closeable;
import java.util.UUID;

/**
 * This interface defines the basic abstractions for a {@link DataSource}
 * which can be used to store and load {@link PlayerData}.
 */
public interface DataSource extends Closeable {

  /**
   * This method will be called before using the {@link DataSource}.
   * It can be used to make the {@link DataSource} ready to use.
   */
  default void setUp() {

  }

  /**
   * Returns the {@link PlayerData} stored for the given {@link UUID},
   * or {@code null} if no such data has been stored for the provided {@link UUID}.
   *
   * @param uuid the {@link UUID} which represents the {@link org.bukkit.entity.Player}
   * @return the {@link PlayerData} for the given {@link UUID} if it exists,
   *         {@code null} otherwise
   */
  PlayerData getData(UUID uuid);

  /**
   * Saves the {@link PlayerData} on the {@link DataSource}.
   *
   * @param playerData the {@link PlayerData} to be saved
   */
  void save(PlayerData playerData);
}
