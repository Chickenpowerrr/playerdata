package com.gmail.chickenpowerrr.playerdata.data;

import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import com.gmail.chickenpowerrr.playerdata.data.source.DataSource;
import com.gmail.chickenpowerrr.playerdata.util.Util;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * This manager handles the save and load mechanics of
 * {@link PlayerData} for the {@link Player}s on the server.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class DataManager implements Closeable {

  private final Object lock;
  private final Plugin plugin;
  private final DataSource dataSource;
  private final ExecutorService executorService;
  private final Map<UUID, Future<Void>> loading;
  private final Map<UUID, Future<Void>> saving;

  /**
   * Instantiates a new manager based on the {@link DataSource} which
   * will be used to persitently save the data.
   *
   * @param plugin the {@link Plugin} which is running this manager
   * @param dataSource the {@link DataSource} which can save the data
   */
  public DataManager(Plugin plugin, DataSource dataSource) {
    this.plugin = plugin;
    this.dataSource = dataSource;

    this.lock = new Object();
    this.loading = new HashMap<>();
    this.saving = new HashMap<>();
    this.executorService = Executors.newCachedThreadPool();
  }

  /**
   * Saves the {@link PlayerData} if the data isn't being saved or loaded at
   * this moment. This will be done asynchronously.
   *
   * @param playerData the {@link PlayerData} to be saved
   */
  public void save(PlayerData playerData) {
    if (!isBusy(playerData.getUuid())) {
      this.saving.computeIfAbsent(playerData.getUuid(), u -> CompletableFuture.supplyAsync(() -> {
        try {
          this.dataSource.save(playerData);

          synchronized (this.lock) {
            this.saving.remove(playerData.getUuid());
          }
        } catch (Exception e) {
          e.printStackTrace();
        }

        return null;
      }, this.executorService));
    }
  }

  /**
   * Loads the {@link PlayerData} for a given {@link Plugin}
   * if the data isn't being saved or loaded at this moment.
   * If it is still performing a task, the program will wait
   * until this task has been done. The load action will be
   * performed when nothing else is loading it.
   * This will be done asynchronously.
   *
   * @param player the {@link Player} whose data should be loaded
   *               and applied
   */
  public void load(Player player) {
    CompletableFuture<Void> completableFuture = new CompletableFuture<>();

    this.executorService.submit(() -> {
      try {
        // Check if something else is running for this player
        Future<Void> task = getTask(player.getUniqueId());
        if (task != null) {
          Util.sneakyExecute(task::get);
        }

        synchronized (this.lock) {
          if (!this.loading.containsKey(player.getUniqueId())) {
            // Claim a loading
            this.loading.put(player.getUniqueId(), completableFuture);
          } else {
            // The player is already getting loaded
            completableFuture.complete(null);
            return;
          }
        }

        // Actually load and apply the data
        apply(player, this.dataSource.getData(player.getUniqueId()));

        // Release the load
        synchronized (this.lock) {
          this.loading.remove(player.getUniqueId());
        }

        completableFuture.complete(null);
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

  /**
   * Returns {@code true} if the {@link Player} has already
   * a load or save task running for them, {@code false} otherwise.
   *
   * @param uuid the {@link UUID} of the {@link Player}
   * @return {@code true} if the {@link Player} has already
   *         a load or save task running for them, {@code false} otherwise
   */
  public boolean isBusy(UUID uuid) {
    synchronized (this.lock) {
      return this.saving.containsKey(uuid) || this.loading.containsKey(uuid);
    }
  }

  /**
   * Let's the {@link ExecutorService} finish the running tasks to make
   * sure the data doesn't get lost and closes the {@link DataSource}
   * after that.
   *
   * @throws IOException if something goes wrong
   */
  @Override
  public void close() throws IOException {
    this.executorService.shutdown();
    this.dataSource.close();
  }

  /**
   * Applies the given {@link PlayerData} to the given {@link Player}.
   *
   * @param player the {@link Player} who will receive the data
   * @param playerData the loaded {@link PlayerData}
   */
  private void apply(Player player, PlayerData playerData) {
    if (playerData != null) {
      Bukkit.getScheduler().runTask(this.plugin, () -> {
        player.setGameMode(playerData.getGameMode());
        player.setAllowFlight(playerData.isFlying());
        player.setStatistic(Statistic.PLAY_ONE_TICK,
            (int) Util.secondsToTicks(playerData.getTimePlayed()));
        player.teleport(playerData.getLocation());
      });
    }
  }

  /**
   * Returns the load or save task running for the given {@link Player}
   * if there is a task running at the moment of the request.
   *
   * @param uuid the {@link UUID} of the {@link Player}
   * @return the load or save task running for the given {@link Player}
   *         if there is a task running at the moment of the request
   */
  private Future<Void> getTask(UUID uuid) {
    synchronized (this.lock) {
      if (this.saving.containsKey(uuid)) {
        return this.saving.get(uuid);
      } else if (this.loading.containsKey(uuid)) {
        return this.loading.get(uuid);
      }
    }

    return null;
  }
}
