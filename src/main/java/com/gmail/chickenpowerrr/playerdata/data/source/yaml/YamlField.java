package com.gmail.chickenpowerrr.playerdata.data.source.yaml;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

/**
 * This class represents a Field in the Yaml to simplify loading a Yaml file.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class YamlField {

  private final String key;
  private final Collection<Object> lastLayer;
  private final Collection<Map<String, Object>> nextLayer;

  /**
   * Loads all of the data the {@link YamlField} contains.
   *
   * @param key the key used to access this {@link YamlField}
   * @param value the value of the {@link YamlField}
   */
  @SuppressWarnings("unchecked")
  YamlField(String key, Object value) {
    this.key = key;

    Collection<Object> lastLayer = new HashSet<>();
    Collection<Map<String, Object>> nextLayer = new HashSet<>();

    if (value instanceof Map) {
      nextLayer.add((Map<String, Object>) value);
    } else {
      lastLayer.add(value);
    }

    this.lastLayer = lastLayer.size() > 0 ? lastLayer : null;
    this.nextLayer = nextLayer.size() > 0 ? nextLayer : null;
  }

  /**
   * Returns the key of this field in the {@link YamlFile}.
   */
  public String getKey() {
    return this.key;
  }

  /**
   * Returns the {@link Object}s that don't have sub-values.
   */
  public Collection<Object> getLastLayer() {
    return this.lastLayer != null ? Collections.unmodifiableCollection(this.lastLayer) : null;
  }

  /**
   * Returns child {@link YamlField}s of this {@link YamlField}.
   */
  public Collection<Map<String, Object>> getNextLayer() {
    return this.nextLayer != null ? Collections.unmodifiableCollection(this.nextLayer) : null;
  }
}
