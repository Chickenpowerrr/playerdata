package com.gmail.chickenpowerrr.playerdata.data.source.yaml;

import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import com.gmail.chickenpowerrr.playerdata.data.player.StoredPlayerData;
import com.gmail.chickenpowerrr.playerdata.data.source.DataSource;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * This class is a {@link DataSource} which uses the Yaml
 * format to store its data. It will save it's data to the
 * file periodically.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class YamlDataSource implements DataSource {

  private final Future<?> updater;
  private final YamlFile yamlFile;

  /**
   * Creates a new {@link DataSource} based on the {@link YamlFile} which will be
   * used to store and retrieve the data.
   *
   * @param yamlFile the representation of the Yaml file on the disk
   */
  public YamlDataSource(YamlFile yamlFile) {
    this.yamlFile = yamlFile;

    this.updater = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
      synchronized (this.yamlFile) {
        this.yamlFile.save();
      }
    }, 10, 10, TimeUnit.SECONDS);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PlayerData getData(UUID uuid) {
    synchronized (this.yamlFile) {
      if (this.yamlFile.getValue(uuid + ".world") != null) {
        return new StoredPlayerData(uuid, this.yamlFile.getValue(uuid + ".world"),
            this.yamlFile.getValue(uuid + ".x"),
            this.yamlFile.getValue(uuid + ".y"),
            this.yamlFile.getValue(uuid + ".z"),
            ((Double) this.yamlFile.getValue(uuid + ".pitch")).floatValue(),
            ((Double) this.yamlFile.getValue(uuid + ".yaw")).floatValue(),
            this.yamlFile.getValue(uuid + ".game-mode"),
            ((Integer) this.yamlFile.getValue(uuid + ".time-played")).longValue(),
            this.yamlFile.getValue(uuid + ".is-flying"));
      } else {
        return null;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void save(PlayerData playerData) {
    synchronized (this.yamlFile) {
      this.yamlFile.setValue(playerData.getUuid() + ".world",
          playerData.getLocation().getWorld().getName());
      this.yamlFile.setValue(playerData.getUuid() + ".x", playerData.getLocation().getX());
      this.yamlFile.setValue(playerData.getUuid() + ".y", playerData.getLocation().getY());
      this.yamlFile.setValue(playerData.getUuid() + ".z", playerData.getLocation().getZ());
      this.yamlFile.setValue(playerData.getUuid() + ".pitch", playerData.getLocation().getPitch());
      this.yamlFile.setValue(playerData.getUuid() + ".yaw", playerData.getLocation().getYaw());
      this.yamlFile.setValue(playerData.getUuid() + ".game-mode", playerData.getGameMode().name());
      this.yamlFile.setValue(playerData.getUuid() + ".time-played", playerData.getTimePlayed());
      this.yamlFile.setValue(playerData.getUuid() + ".is-flying", playerData.isFlying());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void close() {
    this.updater.cancel(false);

    synchronized (this.yamlFile) {
      this.yamlFile.save();
    }
  }
}
