package com.gmail.chickenpowerrr.playerdata.data.source.sql;

import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import com.gmail.chickenpowerrr.playerdata.data.player.StoredPlayerData;
import com.gmail.chickenpowerrr.playerdata.data.source.DataSource;
import com.gmail.chickenpowerrr.playerdata.util.Util;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/**
 * This class is a {@link DataSource} which uses a SQL
 * database to store and load the {@link PlayerData}.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class SqlDataSource implements DataSource {

  private final HikariDataSource dataSource;

  /**
   * Instantiates the {@link javax.sql.DataSource} to be able to create
   * {@link Connection}s to the SQL server in the future.
   *
   * @param host the IP of the SQL server
   * @param port the port of the SQL server
   * @param userName the username on the SQL server
   * @param password the password on the SQL server
   * @param database the name of the database on the SQL server
   * @param maxPoolSize the maximum amount of {@link Connection}s
   *                    which the code should be able to make to
   *                    the SQL server
   */
  public SqlDataSource(String host, int port, String userName, String password, String database,
      int maxPoolSize) {
    this.dataSource = new HikariDataSource();
    this.dataSource.setDataSourceClassName("com.mysql.cj.jdbc.MysqlDataSource");

    this.dataSource.addDataSourceProperty("serverName", host);
    this.dataSource.addDataSourceProperty("port", port);
    this.dataSource.setUsername(userName);
    this.dataSource.setPassword(password);
    this.dataSource.addDataSourceProperty("databaseName", database);
    this.dataSource.setMaximumPoolSize(maxPoolSize);
  }

  /**
   * Creates the tables which will be used to store the data.
   */
  @Override
  public void setUp() {
    try (Connection connection = this.dataSource.getConnection();
        Statement statement = connection.createStatement()) {
      statement.execute("create table if not exists player_data\n"
          + "(\n"
          + "    uuid        char(36)                                                not null\n"
          + "        primary key,\n"
          + "    world       tinytext                                                not null,\n"
          + "    x           double                                                  not null,\n"
          + "    y           double                                                  not null,\n"
          + "    z           double                                                  not null,\n"
          + "    pitch       float                                                   not null,\n"
          + "    yaw         float                                                   not null,\n"
          + "    game_mode   enum ('CREATIVE', 'SURVIVAL', 'ADVENTURE', 'SPECTATOR') not null,\n"
          + "    time_played bigint                                                  not null,\n"
          + "    is_flying   tinyint(1)                                              not null\n"
          + ");");
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PlayerData getData(UUID uuid) {
    try (Connection connection = this.dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
            "SELECT world, x, y, z, pitch, yaw, game_mode, time_played, is_flying "
                + "FROM player_data WHERE uuid = ?;")) {
      TransactionHandler transactionHandler = new TransactionHandler(connection);
      // We make sure only one thing is executing at the same time, thus we can
      // speed up the queries by updating the isolation level
      transactionHandler.apply(Connection.TRANSACTION_READ_UNCOMMITTED, true);

      preparedStatement.setString(1, uuid.toString());

      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        if (resultSet.next()) {
          return new StoredPlayerData(uuid, resultSet.getString("world"),
              resultSet.getDouble("x"), resultSet.getDouble("y"),
              resultSet.getDouble("z"), resultSet.getFloat("pitch"),
              resultSet.getFloat("yaw"), resultSet.getString("game_mode"),
              resultSet.getLong("time_played"),
              resultSet.getBoolean("is_flying"));
        } else {
          return null;
        }
      } finally {
        transactionHandler.revert();
      }
    } catch (SQLException e) {
      return Util.sneakyThrow(e);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void save(PlayerData playerData) {
    try (Connection connection = this.dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
            "INSERT INTO player_data "
                + "(uuid, world, x, y, z, pitch, yaw, game_mode, time_played, is_flying) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE "
                + "world = ?, x = ?, y = ?, z = ?, pitch = ?, yaw = ?, game_mode = ?,"
                + "time_played = ?, is_flying = ?;")) {
      TransactionHandler transactionHandler = new TransactionHandler(connection);
      // We make sure only one thing is executing at the same time, thus we can
      // speed up the queries by updating the isolation level
      transactionHandler.apply(Connection.TRANSACTION_READ_UNCOMMITTED, true);

      preparedStatement.setString(1, playerData.getUuid().toString());
      preparedStatement.setString(2, playerData.getLocation().getWorld().getName());
      preparedStatement.setDouble(3, playerData.getLocation().getX());
      preparedStatement.setDouble(4, playerData.getLocation().getY());
      preparedStatement.setDouble(5, playerData.getLocation().getZ());
      preparedStatement.setFloat(6, playerData.getLocation().getPitch());
      preparedStatement.setFloat(7, playerData.getLocation().getYaw());
      preparedStatement.setString(8, playerData.getGameMode().name());
      preparedStatement.setLong(9, playerData.getTimePlayed());
      preparedStatement.setBoolean(10, playerData.isFlying());

      preparedStatement.setString(11, playerData.getLocation().getWorld().getName());
      preparedStatement.setDouble(12, playerData.getLocation().getX());
      preparedStatement.setDouble(13, playerData.getLocation().getY());
      preparedStatement.setDouble(14, playerData.getLocation().getZ());
      preparedStatement.setFloat(15, playerData.getLocation().getPitch());
      preparedStatement.setFloat(16, playerData.getLocation().getYaw());
      preparedStatement.setString(17, playerData.getGameMode().name());
      preparedStatement.setLong(18, playerData.getTimePlayed());
      preparedStatement.setBoolean(19, playerData.isFlying());

      try {
        preparedStatement.execute();
      } finally {
        transactionHandler.revert();
      }
    } catch (SQLException e) {
      Util.sneakyThrow(e);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void close() {
    this.dataSource.close();
  }
}
