package com.gmail.chickenpowerrr.playerdata.data.player;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * This {@link PlayerData} is a snapshot of what the data of
 * the {@link Player} has been at one point. It can be used to
 * go back to this point.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class StoredPlayerData implements PlayerData {

  private final UUID uuid;
  private final Location location;
  private final GameMode gameMode;
  private final long timePlayed;
  private final boolean flying;

  /**
   * Instantiates new {@link PlayerData} based on the values which
   * the {@link Player} had at the time of saving the snapshot.
   *
   * @param uuid the {@link UUID} of the {@link Player}
   * @param world the name of the {@link World} in which the {@link Player} was
   * @param x the x coordinate at which the {@link Player} was
   * @param y the y coordinate at which the {@link Player} was
   * @param z the z coordinate at which the {@link Player} was
   * @param pitch the pitch in which the {@link Player} was looking
   * @param yaw the yaw in which the {@link Player} was looking
   * @param gameMode the {@link GameMode} the {@link Player} had
   * @param timePlayed how long the {@link Player} had played on the server
   *                   in seconds
   * @param flying {@code true} if the {@link Player} was flying,
   *               {@code false} otherwise
   */
  public StoredPlayerData(UUID uuid, String world, double x, double y, double z,
      float pitch, float yaw, String gameMode, long timePlayed, boolean flying) {
    this.uuid = uuid;
    this.location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    this.gameMode = GameMode.valueOf(gameMode);
    this.timePlayed = timePlayed;
    this.flying = flying;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UUID getUuid() {
    return this.uuid;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Location getLocation() {
    return this.location;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public GameMode getGameMode() {
    return this.gameMode;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public long getTimePlayed() {
    return this.timePlayed;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isFlying() {
    return this.flying;
  }
}
