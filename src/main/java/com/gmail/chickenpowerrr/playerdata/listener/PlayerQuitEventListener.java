package com.gmail.chickenpowerrr.playerdata.listener;

import com.gmail.chickenpowerrr.playerdata.data.DataManager;
import com.gmail.chickenpowerrr.playerdata.data.player.LocalPlayerData;
import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * This class handles a quit by saving the player data.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerQuitEventListener implements Listener {

  private final DataManager dataManager;

  /**
   * Instantiates a new {@link Listener} based on the {@link DataManager}
   * which will be used to save the data.
   *
   * @param dataManager the {@link DataManager} which will be used
   *                    to save the data
   */
  public PlayerQuitEventListener(DataManager dataManager) {
    this.dataManager = dataManager;
  }

  /**
   * Saves the {@link PlayerData}.
   *
   * @param event the {@link PlayerQuitEvent} which alerts the {@link Listener}
   *              of the join of the {@link Player}
   */
  @EventHandler
  public void onQuit(PlayerQuitEvent event) {
    this.dataManager.save(new LocalPlayerData(event.getPlayer()));
  }
}
