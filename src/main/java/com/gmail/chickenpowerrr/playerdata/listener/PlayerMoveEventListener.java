package com.gmail.chickenpowerrr.playerdata.listener;

import com.gmail.chickenpowerrr.playerdata.data.DataManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * This class handles a move by checking if the {@link Player}
 * is currently being loaded or saved.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerMoveEventListener implements Listener {

  private final DataManager dataManager;

  /**
   * Instantiates a new {@link Listener} based on the {@link DataManager}
   * which to check if the {@link Player} can move.
   *
   * @param dataManager the {@link DataManager} which will be used
   *                    to check if the {@link Player} can move
   */
  public PlayerMoveEventListener(DataManager dataManager) {
    this.dataManager = dataManager;
  }

  /**
   * Disallows movement if the {@link Player} is being saved/loaded.
   *
   * @param event the {@link PlayerMoveEvent} which alerts the {@link Listener}
   *              of the movement by the {@link Player}
   */
  @EventHandler(ignoreCancelled = true)
  public void onMove(PlayerMoveEvent event) {
    event.setCancelled(this.dataManager.isBusy(event.getPlayer().getUniqueId()));
  }
}
