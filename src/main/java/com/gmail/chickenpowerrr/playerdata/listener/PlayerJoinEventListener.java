package com.gmail.chickenpowerrr.playerdata.listener;

import com.gmail.chickenpowerrr.playerdata.data.DataManager;
import com.gmail.chickenpowerrr.playerdata.data.player.PlayerData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * This class handles a join by loading the player data.
 *
 * @author Mark van Wijk
 * @since 1.0.0
 */
public class PlayerJoinEventListener implements Listener {

  private final DataManager dataManager;

  /**
   * Instantiates a new {@link Listener} based on the {@link DataManager}
   * which will be used to load the data.
   *
   * @param dataManager the {@link DataManager} which will be used
   *                    to load the data
   */
  public PlayerJoinEventListener(DataManager dataManager) {
    this.dataManager = dataManager;
  }

  /**
   * Loads the {@link PlayerData}.
   *
   * @param event the {@link PlayerJoinEvent} which alerts the {@link Listener}
   *              of the join of the {@link Player}
   */
  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    this.dataManager.load(event.getPlayer());
  }
}
